﻿/// <reference path="../scripts/typings/angularjs/angular.d.ts" />
'use strict';
var Application;
(function (Application) {
    var app = angular.module('app');

    // Collect the routes
    app.constant('routes', getRoutes());

    // Configure the routes and route resolvers
    app.config([
        '$routeProvider', 'routes',
        function ($routeProvider, routes) {
            return new RouteConfigurator($routeProvider, routes);
        }
    ]);

    var RouteConfigurator = (function () {
        function RouteConfigurator($routeProvider, routes) {
            routes.forEach(function (r) {
                $routeProvider.when(r.url, r.config);
            });
            $routeProvider.otherwise({ redirectTo: '/' });
        }
        return RouteConfigurator;
    })();
    Application.RouteConfigurator = RouteConfigurator;

    //Define the routes
    function getRoutes() {
        return [
            {
                url: '/',
                config: {
                    templateUrl: 'app/dashboard/dashboard.html',
                    title: 'dashboard',
                    settings: {
                        nav: 1,
                        content: '<i class="fa fa-dashboard"></i> Dashboard'
                    }
                }
            }, {
                url: '/admin',
                config: {
                    title: 'admin',
                    templateUrl: 'app/admin/admin.html',
                    settings: {
                        nav: 2,
                        content: '<i class="fa fa-lock"></i> Admin'
                    }
                }
            }
        ];
    }
})(Application || (Application = {}));
//# sourceMappingURL=config.route.js.map
