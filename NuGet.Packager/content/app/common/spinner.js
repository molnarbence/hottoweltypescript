﻿/// <reference path="common.ts" />
/// <reference path="commonconfig.ts" />
'use strict';
var Application;
(function (Application) {
    (function (Shared) {
        // Must configure the common service and set its
        // events via the commonConfigProvider
        angular.module('common').factory('spinner', ['common', 'commonConfig', function (common, commonConfig) {
                return new Spinner(common, commonConfig);
            }]);

        var Spinner = (function () {
            function Spinner(common, commonConfig) {
                this.common = common;
                this.commonConfig = commonConfig;
            }
            Spinner.prototype.spinnerHide = function () {
                this.spinnerToggle(false);
            };

            Spinner.prototype.spinnerShow = function () {
                this.spinnerToggle(true);
            };

            Spinner.prototype.spinnerToggle = function (show) {
                this.common.$broadcast(this.commonConfig.config.spinnerToggleEvent, { show: show });
            };
            return Spinner;
        })();
        Shared.Spinner = Spinner;
    })(Application.Shared || (Application.Shared = {}));
    var Shared = Application.Shared;
})(Application || (Application = {}));
//# sourceMappingURL=spinner.js.map
