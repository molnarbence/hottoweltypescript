﻿/// <reference path="../common/common.ts" />
'use strict';
var Application;
(function (Application) {
    (function (Controllers) {
        var controllerId = 'admin';
        angular.module('app').controller(controllerId, ['common', function (common) {
                return new Admin(common, controllerId);
            }]);

        var Admin = (function () {
            //#endregion
            function Admin(common, controllerId) {
                this.common = common;
                this.controllerId = controllerId;
                this.title = "Admin";
                this.log = this.common.logger.getLogFn(controllerId);
                this.activate([]);
            }
            //#region private methods
            Admin.prototype.activate = function (promises) {
                var _this = this;
                this.common.activateController([], controllerId).then(function () {
                    _this.log('Activated Admin View');
                });
            };
            return Admin;
        })();
        Controllers.Admin = Admin;
    })(Application.Controllers || (Application.Controllers = {}));
    var Controllers = Application.Controllers;
})(Application || (Application = {}));
//# sourceMappingURL=admin.js.map
